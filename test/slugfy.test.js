const slugfy = require('../service/slugfy')

test('slugfy specific name with withespaces', () => {
  expect(slugfy('xablau tetudo')).toBe('xablau-tetudo')
})


test('slugfy specific name with withespaces and special char', () => {
    expect(slugfy('xablauzão tétudo')).toBe('xablauzao-tetudo')
  })