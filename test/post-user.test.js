const User = require('../models/user')
const userData = { name: 'Jaozin', email: 'jaozintestao@jao.com', password: 'jaozin12345'}
const connectDB = require('../config/db')

test('save user on database', async () => {
    connectDB()
    const user = new User(userData)
    const savedUser = await user.save()
    expect(savedUser._id).toBeDefined()
    expect(savedUser.name).toBe('Jaozin')
    expect(savedUser.email).toBe('jaozintestao@jao.com')
    expect(savedUser.password).toBe('jaozin12345')
  })

  test('find specific user', async () =>{
    connectDB()
    const id =  '5fa4076cc910de4580437871'
    const user = await User.findById({ _id: id })
    expect(user.name).toBe('Jaozin')
  })