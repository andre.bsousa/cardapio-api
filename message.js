const MSGS = {
    "CATEGORY404": "Categoria não encontrada",
    "CANTDELETECATEGORY": "Não pode deletar uma categoria associada a um produto",
    "CONTENT404": "Conteudo não existe",
    "EMAIL_INVALID": "Email invalido",
    "FILE202": "Arquivo não enviado",
    "FILE400": "Arquivo em formato invalido",
    "GENERIC_ERROR": "Server error",
    "INVALID_TOKEN": "Token invalido",
    "NAME_EMPTY": "Nome vazio, por favor insira seu nome",
    "NO_TOKEN": "Sem token, acesso não autorizado",
    "PASSWORD_LENGTH": "Senha precisa de no minimo 6 caracteres",
    "PRODUCT404": "Produto não encontrado",
    "USER401": "Senha invalida",
    "USER404": "Usuario não existe",
    "USER409": "Usuario ja existe"
}

module.exports = MSGS