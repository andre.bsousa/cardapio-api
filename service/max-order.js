module.exports = (content, type) => {
    let x = null
    if(type == 'service'){
        x = content.services.service
    }
    if(type == 'infos'){
        x = content.infos
    }
    if(type == 'banner'){
        x = content.banner
    }
    x.sort((a, b)=> { return b.order - a.order })
    return x.length == 0 ? 1 : x[0].order + 1

}