const config = require('config')

module.exports = (content) => {
    const BUCKET_PUBLIC_PATH = process.env.BUCKET_S3_PUBLIC_PATH || config.get('BUCKET_S3_PUBLIC_PATH')
    content.about.photo = `${BUCKET_PUBLIC_PATH}${content.about.photo}`
    if(content.services.service != ''){
    content.services.service.map( (s) => {
        s.photo = `${BUCKET_PUBLIC_PATH}${s.photo}`
    } )
    content.banner.map( (s) => {
        s.photo = `${BUCKET_PUBLIC_PATH}${s.photo}`
    })
    content.infos.map( (s) => {
        s.photo = `${BUCKET_PUBLIC_PATH}${s.photo}`
    })
    return content
} else {
    return content
}
}