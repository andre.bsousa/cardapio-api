const AWS = require('aws-sdk')
const MSGS = require('../message')
const fs = require('fs')
const slugify = require('../service/slugfy')
const config = require('config')

module.exports = async (req, res, next) => {
    try {
        const BUCKET_NAME = process.env.BUCKET_S3_NAME || config.get('BUCKET_S3_NAME')
        const s3 = new AWS.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY || config.get('AWS_ACCESS_KEY'),
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || config.get('AWS_SECRET_ACCESS_KEY')
        })
        
        if(!req.files){
            if(req.method == 'PATCH'){
                next()
            } else {
                res.status(204).send({ msg: MSGS.FILE202 })
            }     
            
        } else {
            let photo = req.files['about.photo']
            const name = slugify(photo.name)

            if(photo.mimetype.includes('image/')){
                const file = await phot.mv(`./uploads/${name}`)
                const params = {
                    BucketName: BUCKET_NAME,
                    ACL: 'public-read',
                    Key: `about/${name}`,
                    Body: fs.createReadStream(`./uploads/${name}`)
                }

                s3.upload(params, (err, data) => {
                    if(err){
                        console.err(err)
                        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
                    } else {
                        console.log(`Arquivo gravado com sucesso em: ${data.Location}`)
                        fs.unlinkSync(`./uploads/${name}`)
                        req.body['about.photo'] = `about/${name}`
                        next()
                    }
    
                })
            } else {
                console.error(err.message)
                res.status(400).send({ msg: MSGS.FILE400 })
            }
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
}