const jwt = require('jsonwebtoken')
const config = require('config')
const getSecret = process.env.JWT_SECRET || config.get('JWT_SECRET')
const MSGS = require('../message')


module.exports = (req, res, next) => {    
    const token = req.header('x-auth-token')
    if(!token){
        return res.status(401).send({ "error": MSGS.NO_TOKEN })
    }
    
    try{

        jwt.verify(token, getSecret, (error, decoded) => {
            if(error){
                return res.status(401).send({ "error": MSGS.INVALID_TOKEN })
            } else {
                req.user = decoded.user
                next()
            }
    
        })

    } catch (error){
        console.error(error.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
        
    }


}