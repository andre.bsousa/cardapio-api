const MSGS = require('../message')

module.exports = (req, res , next) => {
    try {
        if(!req.files){
            res.status(204).send({ msg: MSGS.FILE204 })
        } else {
            let photo = req.files.photo
            if(photo.mimetype.includes('image/')){
                photo.mv('./uploads/' + photo.name)
                next()
            }  else {
                res.status(400).send({ msg: MSGS.FILE400 })
            }
        }
        
    } catch (error) {
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
}