const MSGS = require('../message')
const fs = require('fs')
const AWS = require('aws-sdk')
const config = require('config')
const slugify = require('../service/slugfy')

module.exports = async (req, res , next) => {
    try {
        const BUCKET_NAME = process.env.BUCKET_S3_NAME || config.get('BUCKET_S3_NAME')

        const s3 = new AWS.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID || config.get('AWS_ACCESS_KEY_ID'),
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || config.get('AWS_SECRET_ACCESS_KEY')
        })
        const folder = req.baseUrl.replace("/", "")
        if(!req.files){
            if(req.method == 'PATCH'){
                next()
            } else {
                res.status(204).send({ msg: MSGS.FILE204 })
            }
        } else {
            let photo = req.files.photo
            const name = slugify(photo.name)
            req.body.photo_name = name            
            if(photo.mimetype.includes('image/')){
               const file = await photo.mv(`./uploads/${name}`)
               const params = {
                   Bucket: BUCKET_NAME,
                   ACL: 'public-read',
                   Key: `${folder}/${name}`,
                   Body: fs.createReadStream(`./uploads/${name}`)
               }
               s3.upload(params, (error, data) => {
                   if(error){
                       console.error(error)
                       res.status(500).send(error)
                   } else {
                       console.log(`Dado uploads com sucesso em: ${data.Location}`)
                       fs.unlinkSync(`./uploads/${name}`)
                        next()
                   }
               })
            }  else {
                res.status(400).send({ msg: MSGS.FILE400 })
            }
        }
        
    } catch (error) {
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
}