const mongoose = require('mongoose')

const opts = {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
}

const ProductSchema = new mongoose.Schema({
    photo: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    category: {
        type: mongoose.Schema.ObjectId,
        ref: "category",
        required: true
    },
    spotlight: {
        type: Boolean,
        default: false
    },
    description: {
        type: String,
        required: true
    },
    complete_description: {
        type: String
    },
    price: {
        type: Number,
        min: 1,
        required: true
    },
    discount_price: {
        type: Number,
        min: 1
    },
    discount_price_percent: {
        type: Number,
        min: 1
    },
    last_modified_by: {
        type: mongoose.Schema.ObjectId,
        ref: "user"        
    },
    status: {
        type: Boolean,
        default: true
    }

}, opts)

module.exports = mongoose.model('product', ProductSchema)