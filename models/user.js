const mongoose = require('mongoose')

const opts = {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
}

const UserSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true,
        unique: true
    },

    password: {
        type: String,
        required: true,
        select: false
    }
}, opts)

module.exports = mongoose.model('user', UserSchema)