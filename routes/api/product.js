const express = require('express')
const router = express.Router()
const Product = require('../../models/product')
const file = require('../../middleware/file')
const auth = require('../../middleware/auth')
const MSGS = require('../../message')
const config = require('config')
const { check, validationResult } = require('express-validator')
const BUCKET_PUBLIC_PATH = process.env.BUCKET_S3_PUBLIC_PATH || config.get('BUCKET_S3_PUBLIC_PATH')


// @route    POST /product
// @desc     CREATE product
// @access   Private
router.post('/', auth, file, async (req, res, next) => {
    try {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({ erros : errors.array() })
        } else {
        
        req.body.photo = `product/${req.body.photo_name}`
        let product = new Product(req.body)
        product.last_modified_by = req.user.id
        await product.save()
        if(product.id){
            product.photo = `${BUCKET_PUBLIC_PATH}${product.photo}`
            res.json(product)
        }
    }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

// @route    GET /product
// @desc     LIST products
// @access   Public
router.get('/', async (req, res, next) => {
    try {
        let product = await Product.find(req.query).populate('category')
        product.map( (products) => {
            products.photo = `${BUCKET_PUBLIC_PATH}${products.photo}`
            return products
        })
        if(product){
            res.json(product)
        } else {
            res.status(204).send({ msg: MSGS.CONTENT204 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

// @route    GET /product/:productId
// @desc     DETAIL product
// @access   Public
router.get('/:productId', async (req, res, next) => {
    try {
        const product = await Product.findById({ _id: req.params.productId })
        if(product){
            res.json(product)
        } else {
            res.status(404).send({ msg: MSGS.PRODUCT404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })        
    }
})

// @route    PATCH /product/:productId
// @desc     PARTIAL UPDATE product
// @access   Private
router.patch('/:productId', auth, file,  async (req, res, next) => {
    try {
        req.body.last_modified_by = req.user.id
        if(req.body.photo_name){
            req.body.photo = `product/${req.body.photo_name}`
        }
        const id = req.params.productId
        const update = { $set: req.body }
        const product = await Product.findByIdAndUpdate(id, update, { new: true })
        if(product){
            res.json(product)
        } else {
            res.status(404).send({ msg: MSGS.PRODUCT404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }

})

// @route    DELETE /product/:productId
// @desc     DELETE product
// @access   Private
router.delete('/:productId', auth, async (req, res, next) => {
    try {
        const id = req.params.productId
        const product = await Product.findByIdAndDelete({ _id: id })
        if(product){
            res.json(product)
        } else {
            res.status(404).send({ msg: MSGS.PRODUCT404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })        
    }
})

module.exports = router