const express = require('express')
const Content = require('../../models/content')
const router = express.Router()
const auth = require('../../middleware/auth')
const MSGS = require('../../message')
const file = require('../../middleware/file')
const complete_link = require('../../service/complete-link')
const max_order = require('../../service/max-order')


// @route    POST /services/:contentId
// @desc     POST services
// @access   Private
router.post('/contentId', auth, file, async (req, res, next) => {
    try {

        const id = req.params.contentId
        if(req.body.photo_name){
            req.body.photo = `services/${req.body.photo_name}`
        }

        let content = await Content.findOne({ _id: id })
        if(!req.body.order){
            req.body.order = max_order(content, 'service')
        }

        content = await Content.findOneAndUpdate({ _id: id }, { $push: {'services.service': req.body } }, { new: true })
        if(content){
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })        
    }
})

// @route    PATCH /services/:contentId-:serviceId
// @desc     PATCH services
// @access   Private
router.patch('/contentId-:serviceId', auth, file, async (req, res, next) => {
    try {

        const id = req.params.contentId
        const serviceId = req.params.serviceId

        let query = { 'service.services._id': req.body }
        let update = {}
        if(req.body.photo_name){
            req.body.photo = `services/${req.body.photo_name}`
        }
        for(const [key, value] of Object.entries(req.body)){
            update[`service.services.$.${key}`] = value
        }

        await Content.updateOne(query, { $set: update }, { new: true })
        let content = await Content.findOne(query)

        if(content.id){
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }

        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})


// @route    DELETE /services/:contentId
// @desc     DELETE services
// @access   Private
router.delete('/contentId', auth, async (req, res, next) => {
    try {

        const id = req.params.contentId
        let query = { 'service.services': req.body }
        let content = await Content.findByIdAndUpdate( { _id: id }, { $pull: query }, { new: true })
        if(content){
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CATEGORY404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

module.exports = router