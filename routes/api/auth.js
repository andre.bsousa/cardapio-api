const express = require('express')
const router = express.Router()
const User = require('../../models/user')
const bcrypt = require('bcrypt')
const config = require('config')
const jwt = require('jsonwebtoken')
const MSGS = require('../../message')
const { check, validationResult } = require('express-validator')
const getSecret = process.env.JWT_SECRET || config.get('JWT_SECRET')


router.post('/', [
    check("email", MSGS.EMAIL_INVALID ).isEmail(),
    check("password", MSGS.PASSWORD_LENGTH).isLength({ min: 6 })
], async (req, res, next) => {

    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(400).json({ erros : errors.array() })
    }

    const { email, password } = req.body

    try {

        const user = await User.findOne({ email }).select('id password name')
        if(!user){
            return res.status(404).send({ "error": MSGS.USER404 })
        } else {

            const isMatch = bcrypt.compare(password, user.password)
            if(!isMatch){
                return res.status(401).json({ "error": MSGS.USER401 })
            } else {
                const payload = {
                    user: {
                        id: user.id,
                        name: user.name,
                        email: user.email
                    }

                }

                jwt.sign(payload, getSecret, { expiresIn: '5 days' }, (err, token) => {
                    if(err) throw err
                    res.json({ token })
                })
            }
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ "erro": MSGS.GENERIC_ERROR })
        
    }
})

module.exports = router