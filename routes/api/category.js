const express = require('express')
const router = express.Router()
const file = require('../../middleware/file')
const Category = require('../../models/category')
const Product = require('../../models/product')
const MSGS = require('../../message')
const auth = require('../../middleware/auth')
const { check, validationResult } = require('express-validator')

// @route    POST /category
// @desc     CREATE category
// @access   Private
router.post('/', auth, async (req, res, next) => {
    try {
        let { name, icon } = req.body
        let category = new Category({ name, icon })
        await category.save()
        if(category.id){
            res.json(category)
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

// @route    GET /category
// @desc     DETAIL ALL category
// @access   Public
router.get('/', async (req, res, next) => {
    try {
        const category = await Category.find({})
        if(category){
            res.json(category)
        } else {
            res.status(404).send({ msg: MSGS.CATEGORY404 })
        }
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

// @route    GET /category/:id
// @desc     DETAIL ONE category
// @access   Public
router.get('/:categoryId', async (req, res, next) => {
    try {
        const id = req.params.categoryId
        const category = await Category.findById({ _id: id })
        if(category){
            res.json(category)
        } else {
            res.status(404).send({ msg: MSGS.CATEGORY404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})
// @route    PATCH /category/:id
// @desc     PARTIAL UPDATE category
// @access   Private
router.patch('/:categoryId', auth, async (req, res, next) => {
    try {
        const category = await Category.findByIdAndUpdate(req.params.categoryId, {$set: req.body }, { new: true })
        if(category){
            res.json(category)
        } else {
            res.status(404).send({ msg: MSGS.CATEGORY404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })        
    }

})

// @route    PATCH /category/:id
// @desc     PARTIAL UPDATE category
// @access   Private
router.delete('/:categoryId', auth, async (req, res, next) => {
    try {
        const product = await Product.find({ category: req.params.categoryId })
        if(product.length > 0){
            res.status(400).send({ msg: MSGS.CANTDELETECATEGORY })
        } else {
            const category = await Category.findByIdAndDelete(req.params.categoryId)
            if(category){
                res.json(category)
            } else {
                res.status(404).send({ msg: MSGS.CATEGORY404 })
            }
        }
       
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }

})

module.exports = router