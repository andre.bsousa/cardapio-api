const express = require('express')
const router = express.Router()
const User = require('../../models/user')
const { check, validationResult } = require('express-validator')
const auth = require('../../middleware/auth')
const MSGS = require('../../message')
const bcrypt = require('bcrypt')

// @route POST/user
// @desc CREATE user
// @acess Public
router.post('/', [
    check('email', MSGS.EMAIL_INVALID).isEmail(),
    check('name', MSGS.NAME_EMPTY).not().isEmpty(),
    check('password', MSGS.PASSWORD_LENGTH).isLength({ min: 6 })
], async (req, res, next) => {
    try{

        let { email, name, password } = req.body
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({ erros : errors.array() })
        } else {
            const isUserExist = await User.findOne({ email })
            if(isUserExist){
                res.status(409).json({ msg: MSGS.USER409 })
            } else {
                let user = new User({ email, name, password })
                const salt = await bcrypt.genSalt(10)
                user.password = await bcrypt.hash(password, salt)
                await user.save()
                if(user.id){
                    res.json(user)
                }
            }
        }

    } catch (error){
        console.error(error.message)
        res.status(500).send( { msg: MSGS.GENERIC_ERROR })
    }
} )

// @route GET/user
// @desc List users
// @acess Private
router.get('/', auth, async (req, res, next) => {
    try {
        const user = await User.find({})
        if(user){
            res.json(user)
        }

    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
    }
})

// @routeGET/user
// @desc List users by ID
// @acess Private 
router.get('/:userId', auth, async (req, res, next) => {
    const id = req.params['userId']
    try {
        const user = await User.findOne({ _id: id })
        if(user){
            res.json(user)
        } else {
            res.status(404).send({ msg: MSGS.USER404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

// @route PATCH/user
// @desc PARCIAL EDIT user
// @acess Private
router.patch('/:userId', auth, async (req, res, next) => {
    const id = req.params['userId']
    try {
        const salt = await bcrypt.genSalt(10)
        let bodyRequest = req.body
        if(bodyRequest.password){
            bodyRequest.password = await bcrypt.hash(bodyRequest.password, salt)
        }

        const update = { $set: bodyRequest }
        const user = await User.findByIdAndUpdate(id, update, { new: true })
        if(user){
            res.send(user)
        } else {
            res.status(404).send({ msg: MSGS.USER404 })
        }



    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
    }

})

// @route DELETE/user
// @desc DELETE user
// @acess Private
router.delete('/:userId', auth, async (req, res, next) => {
    const id = req.params.userId
    try {
        const user = await User.findByIdAndDelete({ _id: id })
        if(user){
            res.send(user)
        } else {
            res.status(404).send({ msg: MSGS.USER404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

module.exports = router
