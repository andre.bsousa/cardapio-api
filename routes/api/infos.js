const express = require('express')
const Content = require('../../models/content')
const router = express.Router()
const auth = require('../../middleware/auth')
const MSGS = require('../../message')
const file = require('../../middleware/file')
const complete_link = require('../../service/complete-link')
const max_order = require('../../service/max-order')

// @route    POST /infos/:contentId
// @desc     CREATE infos
// @access   Private
router.post('/:contentId', auth, file, async (req, res, next) => {
    try {

        const id = req.params.contentId
        if(req.body.photo_name){
            req.body.photo = `infos/${req.body.photo_name}`
        }

        let content = await Content.findOne({ _id: id })
        if(!req.body.order){
            req.body.order = max_order(content, 'infos')
        }

        content = await Content.findOneAndUpdate({ _id: id }, { $push: { infos: req.body } }, { new: true })
        if(content){
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

// @route    PATCH /infos/:contentId-:infoId
// @desc     UPDATE infos
// @access   Private
router.patch('/:contentId-:infoId', auth, file, async (req, res, next) => {
    try {

        const id = req.params.contentId
        const infoId = req.params.infoId

        let query = { 'info._id': infoId }
        if(req.body.photo_name){
            req.body.photo = `infos/${req.body.photo_name}`
        }

        let update = {}
        for(const [key, value] of Object.entries(req.body)){
            update[`infos.$.${key}`] = value
        }
        
        await Content.updateOne(query, {$set: update}, { new: true })
        let content = await Content.findOne(query)
        
        if(content.id){
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

// @route    DELETE /infos/:contentId
// @desc     DELETE infos
// @access   Private
router.delete('/:contentId', auth, async (req, res, next) => {
    try {

        const id = req.params.contentId

        let content = await Content.findOneAndUpdate({ _id: id }, { $pull: { infos: req.body } }, { new: true })
        if(content){
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

module.exports = router

