const express = require('express')
const router = express.Router()
const Content = require('../../models/content')
const MSGS = require('../../message')
const auth = require('../../middleware/auth')
const file = require('../../middleware/file')
const max_order = require('../../service/max-order')
const complete_link = require('../../service/complete-link')


// @route    POST /banner/:contentId
// @desc     CREATE banner
// @access   Private
router.post('/:contentId', auth, file, async (req, res, next) => {
    try {
        const id = req.params.contentId
        if (req.body.photo_name) {
            req.body.photo = `banner/${req.body.photo_name}`
        }
        let content = await Content.findOne({ _id: id })
        if (!req.body.order) {
            req.body.order = max_order(content, 'banner')
        }
        content = await Content.findOneAndUpdate({ _id: id }, { $push: { banner: req.body } }, { new: true })
        if (content) {
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }

    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })

    }
})


// @route    PATCH /banner/:contentId
// @desc     PARTIAL UPDATE banner
// @access   Private
router.patch('/:contentId-:infoId', auth, file, async (req, res, next) => {
    try {
        const contentId = req.params.contentId
        const infoId = req.params.infoId

        let query = { 'banner._id': infoId }
        if (req.body.photo_name) {
            req.body.photo_name = `banner/${req.body.photo_name}`
        }
        let update = {}
        for (const [key, value] of Object.entries(req.body)) {
            update[`banner.$.${key}`] = value
        }

        await Content.updateOne(query, { $set: update }, { new: true })
        let content = await Content.findOne(query)

        if (content.id) {
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }

    } catch (error) { 
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
    }

})

// @route    DELETE /banner/:contentId
// @desc     DELETE banner
// @access   Private
router.delete('/:contentId', auth, async (req, res, next) => {
    try {

        const id = req.params.contentId

        const content = await Content.findOneAndUpdate({ _id: id }, 
            { $pull: { banner: req.body } }, { new: true })
        
        if(content){
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }

    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

module.exports = router