const express = require('express')
const router = express.Router()
const Content = require('../../models/content')
const auth = require('../../middleware/auth')
const file = require('../../middleware/file')
const MSGS = require('../../message')
const complete_link = require('../../service/complete-link')


// @route    POST /content
// @desc     CREATE content
// @access   Private
router.post('/', auth, async (req, res, next) => {
    try {

        let content = new Content(req.body)
        await content.save()
        if(content.id){
            content = complete_link(content)
            res.json(content)
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })        
    }
})

// @route    GET /content
// @desc     LIST content
// @access   Public
router.get('/', async (req, res, next) => {
    try {

        let content = await Content.findOne({}).sort('-last_modification_date')
            content = complete_link(content)
            res.json(content)

    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

// @route    PATCH /content
// @desc     PARTIAL Update content
// @access   Private
router.patch('/:contentId', auth, file, async (req, res, next) => {
    try {

        const id = req.params.contentId
        const update = { $set: req.body }

        let content = await Content.findByIdAndUpdate(id, update, { new: true })
        if(content){
            content = complete_link(content)
            res.json(content)
        } else {
            res.status(404).send({ msg: MSGS.CONTENT404 })
        }
        
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })        
    }
})

// @route    DELETE /content
// @desc     DELETE content
// @access   Private
router.delete('/:contentId', auth, async (req, res, next) => {

    try {

        const id = req.params.contentId

        let content = await Content.findByIdAndDelete({ _id: id })
        if(content){
            content = complete_link(content)
            res.json(content)
        } else {
    
            res.status(404).json({ msg: MSGS.CONTENT404 })
        }
    } catch (error) {
        console.error(error.message)
        res.status(500).send({ msg: MSGS.GENERIC_ERROR })
        
    }
})

module.exports = router