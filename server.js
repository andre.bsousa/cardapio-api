const express = require('express')
const app = express()
const cors = require('cors')
const bodyParse = require('body-parser')
const PORT = process.env.PORT || 8080
const connectDB = require('./config/db')
const fileUpload = require('express-fileupload')


app.use(cors())
app.use(express.json())
app.use(bodyParse.urlencoded({ extended: true }))
app.use(bodyParse.json())
app.use('/uploads', express.static('uploads'))


connectDB()


app.use(fileUpload({
    createParentPath: true
}))


app.use('/', require('./routes/hello'))
app.use('/user', require('./routes/api/user'))
app.use('/auth', require('./routes/api/auth'))
app.use('/product', require('./routes/api/product'))
app.use('/category', require('./routes/api/category'))
app.use('/banner', require('./routes/api/banner'))
app.use('/content', require('./routes/api/content'))
app.use('/infos', require('./routes/api/infos'))
app.use('/service', require('./routes/api/service'))


app.listen(PORT, () => console.log(`Rodando em http://localhost:${PORT}`))